CREATE DATABASE  IF NOT EXISTS `mydb` /*!40100 DEFAULT CHARACTER SET utf8 */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `mydb`;
-- MySQL dump 10.13  Distrib 8.0.22, for Win64 (x86_64)
--
-- Host: localhost    Database: mydb
-- ------------------------------------------------------
-- Server version	8.0.22

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `accident`
--

DROP TABLE IF EXISTS `accident`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `accident` (
  `idaccident` int NOT NULL AUTO_INCREMENT,
  `accidentTypeId` int NOT NULL,
  `accidentCar` int NOT NULL,
  `accidentDate` datetime NOT NULL,
  `accidentLoc` varchar(255) NOT NULL,
  `accidentDiscription` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idaccident`),
  KEY `Car_idx` (`accidentCar`),
  KEY `Type_idx` (`accidentTypeId`),
  CONSTRAINT `accidentCar` FOREIGN KEY (`accidentCar`) REFERENCES `car` (`idcar`),
  CONSTRAINT `accidentType` FOREIGN KEY (`accidentTypeId`) REFERENCES `accidenttype` (`idaccidentType`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accident`
--

LOCK TABLES `accident` WRITE;
/*!40000 ALTER TABLE `accident` DISABLE KEYS */;
INSERT INTO `accident` VALUES (1,1,1,'2020-12-04 00:00:00','Accident Location','Accident Description'),(2,2,1,'2020-12-04 00:00:00','Accident Location','Accident Description'),(3,3,1,'2020-12-04 00:00:00','Accident Location','Accident Description'),(4,1,2,'2020-12-04 00:00:00','Accident Location','Accident Description'),(5,1,3,'2020-12-04 00:00:00','Accident Location','Accident Description'),(6,2,3,'2020-12-04 00:00:00','Accident Location','Accident Description'),(7,1,4,'2020-12-04 00:00:00','Accident Location','Accident Description'),(8,3,4,'2020-12-04 00:00:00','Accident Location','Accident Description'),(9,3,4,'2020-12-04 00:00:00','Accident Location','Accident Description'),(10,2,5,'2020-12-04 00:00:00','Accident Location','Accident Description'),(11,2,5,'2020-12-04 00:00:00','Accident Location','Accident Description'),(12,3,5,'2020-12-04 00:00:00','Accident Location','Accident Description'),(13,2,6,'2020-12-04 00:00:00','Accident Location','Accident Description'),(14,1,6,'2020-12-04 00:00:00','Accident Location','Accident Description'),(15,1,6,'2020-12-04 00:00:00','Accident Location','Accident Description'),(16,2,7,'2020-12-04 00:00:00','Accident Location','Accident Description'),(17,2,7,'2020-12-04 00:00:00','Accident Location','Accident Description'),(18,2,7,'2020-12-04 00:00:00','Accident Location','Accident Description'),(19,1,7,'2020-12-04 00:00:00','Accident Location','Accident Description'),(20,3,7,'2020-12-04 00:00:00','Accident Location','Accident Description'),(21,1,8,'2020-12-04 00:00:00','Accident Location','Accident Description'),(22,3,8,'2020-12-04 00:00:00','Accident Location','Accident Description'),(23,1,9,'2020-12-04 00:00:00','Accident Location','Accident Description'),(24,3,9,'2020-12-04 00:00:00','Accident Location','Accident Description'),(25,2,9,'2020-12-04 00:00:00','Accident Location','Accident Description'),(26,2,9,'2020-12-04 00:00:00','Accident Location','Accident Description'),(27,1,9,'2020-12-04 00:00:00','Accident Location','Accident Description'),(28,1,10,'2020-12-04 00:00:00','Accident Location','Accident Description'),(29,2,10,'2020-12-04 00:00:00','Accident Location','Accident Description'),(30,3,10,'2020-12-04 00:00:00','Accident Location','Accident Description'),(31,1,10,'2020-12-04 00:00:00','Accident Location','Accident Description'),(32,1,11,'2020-12-04 00:00:00','Accident Location','Accident Description'),(33,1,12,'2020-12-04 00:00:00','Accident Location','Accident Description'),(34,2,12,'2020-12-04 00:00:00','Accident Location','Accident Description'),(35,1,12,'2020-12-04 00:00:00','Accident Location','Accident Description');
/*!40000 ALTER TABLE `accident` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `accidenttype`
--

DROP TABLE IF EXISTS `accidenttype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `accidenttype` (
  `idaccidentType` int NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `discribction` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idaccidentType`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accidenttype`
--

LOCK TABLES `accidenttype` WRITE;
/*!40000 ALTER TABLE `accidenttype` DISABLE KEYS */;
INSERT INTO `accidenttype` VALUES (1,'Death & Dismemberment','Accidental death and dismemberment insurance.'),(2,'General Accident Insurance','General Accident.'),(3,'Scratch','Little Accident such as Scratch/bumb.');
/*!40000 ALTER TABLE `accidenttype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `car`
--

DROP TABLE IF EXISTS `car`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `car` (
  `idcar` int NOT NULL AUTO_INCREMENT,
  `carNumber` int NOT NULL,
  `carStateId` int NOT NULL,
  `carYear` int NOT NULL,
  `carModelId` int NOT NULL,
  `carOnerId` int NOT NULL,
  PRIMARY KEY (`idcar`),
  UNIQUE KEY `carNumber` (`carNumber`,`carStateId`),
  KEY `model_idx` (`carModelId`),
  KEY `state_idx` (`carStateId`),
  KEY `carOner_idx` (`carOnerId`),
  CONSTRAINT `carModel` FOREIGN KEY (`carModelId`) REFERENCES `carmodel` (`idcarModel`),
  CONSTRAINT `carOner` FOREIGN KEY (`carOnerId`) REFERENCES `clinet` (`idclinet`),
  CONSTRAINT `carState` FOREIGN KEY (`carStateId`) REFERENCES `state` (`idstate`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `car`
--

LOCK TABLES `car` WRITE;
/*!40000 ALTER TABLE `car` DISABLE KEYS */;
INSERT INTO `car` VALUES (1,123456,1,1,2021,1),(2,441234,1,1,2021,1),(3,948576,1,2,2020,2),(4,222341,1,3,2019,3),(5,123561,1,4,2012,4),(6,985631,1,5,2012,5),(7,664563,1,6,2012,7),(8,552314,1,7,2011,8),(9,231312,1,8,2020,9),(10,123412,1,9,2020,9),(11,213123,1,9,2020,10),(12,342342,1,10,2010,11),(13,123521,1,11,2003,12),(14,123534,1,12,2003,12),(15,456456,1,12,2003,12);
/*!40000 ALTER TABLE `car` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `carmanufacturer`
--

DROP TABLE IF EXISTS `carmanufacturer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `carmanufacturer` (
  `idcarManufacture` int NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `carManufacturerEnable` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`idcarManufacture`),
  UNIQUE KEY `name_UNIQUE` (`name`),
  UNIQUE KEY `name_carUNIQUE` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `carmanufacturer`
--

LOCK TABLES `carmanufacturer` WRITE;
/*!40000 ALTER TABLE `carmanufacturer` DISABLE KEYS */;
INSERT INTO `carmanufacturer` VALUES (1,'Honda',_binary ''),(2,'Toyota',_binary ''),(3,'Chevrolet',_binary ''),(4,'Ford',_binary ''),(5,'Mercedes-Benz',_binary ''),(6,'BMW',_binary ''),(7,'Jeep',_binary '');
/*!40000 ALTER TABLE `carmanufacturer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `carmodel`
--

DROP TABLE IF EXISTS `carmodel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `carmodel` (
  `idcarModel` int NOT NULL AUTO_INCREMENT,
  `carModelManufacturer` int NOT NULL,
  `carModelName` varchar(45) NOT NULL,
  `carModelcolEnable` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`idcarModel`),
  KEY `carManufacturer_idx` (`carModelManufacturer`),
  CONSTRAINT `carManufacturer` FOREIGN KEY (`carModelManufacturer`) REFERENCES `carmanufacturer` (`idcarManufacture`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `carmodel`
--

LOCK TABLES `carmodel` WRITE;
/*!40000 ALTER TABLE `carmodel` DISABLE KEYS */;
INSERT INTO `carmodel` VALUES (1,1,'Civic',_binary ''),(2,1,'Fit',_binary ''),(3,2,'Corolla',_binary ''),(4,2,'Yaris',_binary ''),(5,3,'Camaro',_binary ''),(6,3,'Cruze',_binary ''),(7,3,'Camaro',_binary ''),(8,3,'Cruze',_binary ''),(9,5,'C-Class',_binary ''),(10,5,'A-Class',_binary '');
/*!40000 ALTER TABLE `carmodel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clinet`
--

DROP TABLE IF EXISTS `clinet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `clinet` (
  `idclinet` int NOT NULL AUTO_INCREMENT,
  `clinetname` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `clinetaddres` varchar(255) NOT NULL,
  `clinEtemail` varchar(45) NOT NULL,
  `clinetPhone` int NOT NULL,
  `clinetDrivingLicenseImg` varchar(45) DEFAULT NULL,
  `clinetIdImg` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idclinet`),
  UNIQUE KEY `clinetIdImg_UNIQUE` (`clinetIdImg`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clinet`
--

LOCK TABLES `clinet` WRITE;
/*!40000 ALTER TABLE `clinet` DISABLE KEYS */;
INSERT INTO `clinet` VALUES (1,'Gordie Andrivel','Sicaya','gandrivel0@e-recht24.de',271185265,'auctorGravidaSem.tiff','Nisi.png'),(2,'Sam Harflete','Valenciennes','sharflete1@patch.com',898779941,'volutpat.tiff','VestibulumRutrumRutrum.png'),(3,'Gregoire Dyball','Portobelo','gdyball2@marketwatch.com',582669872,'ante.jpeg','AugueAliquam.png'),(4,'Lucky Szymanski','Gorno Orizari','lszymanski3@virginia.edu',792778689,'sitAmetConsectetuer.jpeg','Proin.tiff'),(5,'Janene Withers','Huazhai','jwithers4@uol.com.br',893465453,'ipsumPrimisIn.jpeg','Duis.tiff'),(6,'Jedidiah Vineall','El Colorado','jvineall5@unesco.org',813155602,'odioCras.jpeg','Vivamus.tiff'),(7,'Gan Fassbindler','Catayauan','gfassbindler6@cdbaby.com',570247556,'convallisNuncProin.png','Odio.tiff'),(8,'Tobiah Strothers','Kiambu','tstrothers7@blinklist.com',726240398,'utBlanditNon.jpeg','Aliquam.tiff'),(9,'Eryn Wretham','Gaoyan','ewretham8@engadget.com',498006446,'ipsum.tiff','Quisque.tiff'),(10,'Marielle Sheddan','Valenciennes','msheddan9@multiply.com',720158064,'congueRisus.tiff','ViverraEget.gif'),(11,'Pieter Teas','San Bernardino','pteasa@e-recht24.de',342742029,'justoPellentesqueViverra.tiff','At.png'),(12,'Mariette Dulin','Mezinovskiy','mdulinb@jiathis.com',706903724,'idMassaId.jpeg','Nulla.tiff');
/*!40000 ALTER TABLE `clinet` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `damage`
--

DROP TABLE IF EXISTS `damage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `damage` (
  `idharm` int NOT NULL AUTO_INCREMENT,
  `damageTypeId` int NOT NULL,
  `damageAmmount` int NOT NULL,
  `damageOnerId` int DEFAULT NULL,
  `harmImg` varchar(45) DEFAULT NULL,
  `damageAccidentId` int NOT NULL,
  `damageCompensationValue` int DEFAULT NULL,
  `harmPolicyId` int DEFAULT NULL,
  `damageCompensationDate` datetime DEFAULT NULL,
  PRIMARY KEY (`idharm`),
  KEY `type_idx` (`damageTypeId`),
  KEY `accident_idx` (`damageAccidentId`),
  KEY `oner_idx` (`damageOnerId`),
  KEY `coverPolicy_idx` (`harmPolicyId`),
  CONSTRAINT `damageAccident` FOREIGN KEY (`damageAccidentId`) REFERENCES `accident` (`idaccident`),
  CONSTRAINT `damageCoverPolicy` FOREIGN KEY (`harmPolicyId`) REFERENCES `insurancepolicy` (`idInsurancePolicy`),
  CONSTRAINT `damageOner` FOREIGN KEY (`damageOnerId`) REFERENCES `clinet` (`idclinet`),
  CONSTRAINT `damageType` FOREIGN KEY (`damageTypeId`) REFERENCES `damagetype` (`iddamageType`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `damage`
--

LOCK TABLES `damage` WRITE;
/*!40000 ALTER TABLE `damage` DISABLE KEYS */;
INSERT INTO `damage` VALUES (1,1,5000,1,'damage.png',1,0,1,'2020-12-04 00:00:00'),(2,2,10000,1,'damage.png',1,10000,1,'2020-12-04 00:00:00'),(3,3,500,2,'damage.png',1,500,1,'2020-12-04 00:00:00'),(4,3,500,3,'damage.png',1,500,2,'2020-12-04 00:00:00'),(5,2,5040,4,'damage.png',1,0,3,'2020-12-04 00:00:00'),(6,2,44444,5,'damage.png',1,0,3,'2020-12-04 00:00:00'),(7,2,5040,5,'damage.png',1,0,3,'2020-12-04 00:00:00'),(8,2,2135,6,'damage.png',1,2135,3,'2020-12-04 00:00:00'),(9,2,5040,7,'damage.png',1,5040,3,'2020-12-04 00:00:00'),(10,2,10000,8,'damage.png',1,0,3,'2020-12-04 00:00:00'),(11,2,400,9,'damage.png',1,32,3,'2020-12-04 00:00:00'),(12,2,7000,10,'damage.png',1,350,3,'2020-12-04 00:00:00'),(13,2,4000,11,'damage.png',1,4000,2,'2020-12-04 00:00:00'),(14,2,8000,10,'damage.png',1,8000,1,'2020-12-04 00:00:00'),(15,2,4000,11,'damage.png',1,0,2,'2020-12-04 00:00:00'),(16,1,5000,11,'damage.png',1,0,2,'2020-12-04 00:00:00'),(17,2,8000,10,'damage.png',1,8000,3,'2020-12-04 00:00:00');
/*!40000 ALTER TABLE `damage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `damagetype`
--

DROP TABLE IF EXISTS `damagetype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `damagetype` (
  `iddamageType` int NOT NULL AUTO_INCREMENT,
  `damageTypeName` varchar(45) NOT NULL,
  `damageTypeDiscribtion` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`iddamageType`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `damagetype`
--

LOCK TABLES `damagetype` WRITE;
/*!40000 ALTER TABLE `damagetype` DISABLE KEYS */;
INSERT INTO `damagetype` VALUES (1,'Scrap','complete vehicle crushed without any components being removed.'),(2,'Break','body-shell/chassis crushed without any structural components being removed.'),(3,'Repairable','Structurally damaged but repairable.');
/*!40000 ALTER TABLE `damagetype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `insurancepolicy`
--

DROP TABLE IF EXISTS `insurancepolicy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `insurancepolicy` (
  `idInsurancePolicy` int NOT NULL AUTO_INCREMENT,
  `InsurancePolicyOner` int NOT NULL,
  `InsurancePolicyTypeId` int NOT NULL,
  `InsurancePolicyCreateDate` datetime NOT NULL,
  `timeSloteId` int NOT NULL,
  PRIMARY KEY (`idInsurancePolicy`),
  KEY `oner_idx` (`InsurancePolicyOner`),
  KEY `type_idx` (`InsurancePolicyTypeId`),
  KEY `timeSlote_idx` (`timeSloteId`),
  CONSTRAINT `policyoner` FOREIGN KEY (`InsurancePolicyOner`) REFERENCES `clinet` (`idclinet`),
  CONSTRAINT `policytimeSlote` FOREIGN KEY (`timeSloteId`) REFERENCES `timeslot` (`idtimeSlot`),
  CONSTRAINT `policytype` FOREIGN KEY (`InsurancePolicyTypeId`) REFERENCES `insurancepolicytype` (`idInsurancePolicyType`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `insurancepolicy`
--

LOCK TABLES `insurancepolicy` WRITE;
/*!40000 ALTER TABLE `insurancepolicy` DISABLE KEYS */;
INSERT INTO `insurancepolicy` VALUES (1,8,3,'2020-02-10 00:00:00',2),(2,1,1,'2020-07-11 00:00:00',2),(3,1,2,'2019-03-09 00:00:00',3),(4,2,2,'2020-12-10 00:00:00',5),(5,3,3,'2020-02-10 00:00:00',5),(6,4,1,'2019-05-10 00:00:00',9),(7,5,2,'2019-12-09 00:00:00',1),(8,6,3,'2020-03-10 00:00:00',2),(9,7,2,'2019-12-10 00:00:00',3),(10,8,3,'2020-03-09 00:00:00',1),(11,9,3,'2019-02-10 00:00:00',9),(12,10,3,'2019-03-10 00:00:00',6),(13,11,1,'2020-12-10 00:00:00',5),(14,11,3,'2020-12-10 00:00:00',3),(15,12,1,'2019-03-10 00:00:00',2),(16,12,2,'2019-03-10 00:00:00',2),(17,12,3,'2019-03-10 00:00:00',2);
/*!40000 ALTER TABLE `insurancepolicy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `insurancepolicytype`
--

DROP TABLE IF EXISTS `insurancepolicytype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `insurancepolicytype` (
  `idInsurancePolicyType` int NOT NULL AUTO_INCREMENT,
  `InsurancePolicyTypeName` varchar(45) NOT NULL,
  `InsurancePolicyTypeDiscribction` varchar(255) NOT NULL,
  PRIMARY KEY (`idInsurancePolicyType`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `insurancepolicytype`
--

LOCK TABLES `insurancepolicytype` WRITE;
/*!40000 ALTER TABLE `insurancepolicytype` DISABLE KEYS */;
INSERT INTO `insurancepolicytype` VALUES (1,'Liability','Covers Bodily/Property Damage.'),(2,'Collision','Covers You/Passengers\'s hospital visits, surgery and x-rays.'),(3,'Personal Injury','Covers Your medical expenses after an accident.');
/*!40000 ALTER TABLE `insurancepolicytype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `policy_accidant_damagetype`
--

DROP TABLE IF EXISTS `policy_accidant_damagetype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `policy_accidant_damagetype` (
  `damageTypeId` int NOT NULL,
  `policyTypeId` int NOT NULL,
  `accidantTypeId` int NOT NULL,
  `personalDamage` bit(1) NOT NULL,
  PRIMARY KEY (`damageTypeId`,`policyTypeId`,`accidantTypeId`,`personalDamage`),
  KEY `policyAccidantHarm_idx` (`damageTypeId`),
  KEY `AccidantType_idx` (`accidantTypeId`),
  KEY `policyType_idx` (`policyTypeId`),
  CONSTRAINT `PADaccidantDamageType` FOREIGN KEY (`damageTypeId`) REFERENCES `damagetype` (`iddamageType`),
  CONSTRAINT `PADaccidantType` FOREIGN KEY (`accidantTypeId`) REFERENCES `accidenttype` (`idaccidentType`),
  CONSTRAINT `PADpolicyType` FOREIGN KEY (`policyTypeId`) REFERENCES `insurancepolicytype` (`idInsurancePolicyType`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `policy_accidant_damagetype`
--

LOCK TABLES `policy_accidant_damagetype` WRITE;
/*!40000 ALTER TABLE `policy_accidant_damagetype` DISABLE KEYS */;
/*!40000 ALTER TABLE `policy_accidant_damagetype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `policy_car`
--

DROP TABLE IF EXISTS `policy_car`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `policy_car` (
  `idpolicy_car` int NOT NULL AUTO_INCREMENT,
  `policyId` int NOT NULL,
  `carId` int NOT NULL,
  PRIMARY KEY (`idpolicy_car`),
  KEY `car_idx` (`carId`),
  KEY `policy_idx` (`policyId`),
  CONSTRAINT `PCcar` FOREIGN KEY (`carId`) REFERENCES `car` (`idcar`),
  CONSTRAINT `PCpolicy` FOREIGN KEY (`policyId`) REFERENCES `insurancepolicy` (`idInsurancePolicy`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `policy_car`
--

LOCK TABLES `policy_car` WRITE;
/*!40000 ALTER TABLE `policy_car` DISABLE KEYS */;
/*!40000 ALTER TABLE `policy_car` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `premiums`
--

DROP TABLE IF EXISTS `premiums`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `premiums` (
  `PremiumsPolicyId` int NOT NULL,
  `PremiumsValue` float NOT NULL,
  `PremiumsMadeDate` datetime DEFAULT NULL,
  `PremiumscDueDate` datetime NOT NULL,
  `timeSloteId` int NOT NULL,
  PRIMARY KEY (`PremiumsPolicyId`,`timeSloteId`),
  UNIQUE KEY `policy_timeSlot_uniq` (`PremiumsPolicyId`,`timeSloteId`),
  KEY `Policy_idx` (`PremiumsPolicyId`),
  KEY `timeSlote_idx` (`timeSloteId`),
  CONSTRAINT `PremiumsPolicy` FOREIGN KEY (`PremiumsPolicyId`) REFERENCES `insurancepolicy` (`idInsurancePolicy`),
  CONSTRAINT `PremiumsTimeSlote` FOREIGN KEY (`timeSloteId`) REFERENCES `timeslot` (`idtimeSlot`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `premiums`
--

LOCK TABLES `premiums` WRITE;
/*!40000 ALTER TABLE `premiums` DISABLE KEYS */;
INSERT INTO `premiums` VALUES (1,5000,NULL,'2020-03-10 00:00:00',2),(2,1000,'2020-07-11 00:00:00','2021-07-11 00:00:00',2),(3,2500,'2019-03-09 00:00:00','2020-03-09 00:00:00',3),(4,2500,NULL,'2021-12-10 00:00:00',5),(5,5000,'2020-02-10 00:00:00','2021-02-10 00:00:00',5),(6,1000,NULL,'2020-05-10 00:00:00',9),(7,2500,'2019-12-09 00:00:00','2020-12-09 00:00:00',1),(8,5000,NULL,'2021-03-10 00:00:00',2),(9,2500,NULL,'2021-12-10 00:00:00',3),(10,5000,'2020-03-09 00:00:00','2021-03-09 00:00:00',1),(11,5000,'2019-02-10 00:00:00','2020-02-10 00:00:00',9),(12,5000,NULL,'2020-03-10 00:00:00',6),(13,1000,NULL,'2021-12-10 00:00:00',5),(14,5000,NULL,'2021-12-10 00:00:00',3),(15,1000,'2019-03-10 00:00:00','2020-03-10 00:00:00',2),(16,2500,NULL,'2020-03-10 00:00:00',2),(17,5000,'2019-03-10 00:00:00','2020-03-10 00:00:00',2);
/*!40000 ALTER TABLE `premiums` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `returns`
--

DROP TABLE IF EXISTS `returns`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `returns` (
  `ReturnsValue` float NOT NULL,
  `ReturnsDueDate` datetime NOT NULL,
  `ReturnsReceipt` datetime NOT NULL,
  `ReturnsPolicy` int NOT NULL,
  `timeSloteId` int NOT NULL,
  UNIQUE KEY `policy_timeSlot_uniq` (`ReturnsPolicy`,`timeSloteId`),
  KEY `policy_idx` (`ReturnsPolicy`),
  KEY `timeSlote_idx` (`timeSloteId`),
  CONSTRAINT `ReturnsPolicy` FOREIGN KEY (`ReturnsPolicy`) REFERENCES `insurancepolicy` (`InsurancePolicyOner`),
  CONSTRAINT `ReturnsTimeSlote` FOREIGN KEY (`timeSloteId`) REFERENCES `timeslot` (`idtimeSlot`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `returns`
--

LOCK TABLES `returns` WRITE;
/*!40000 ALTER TABLE `returns` DISABLE KEYS */;
INSERT INTO `returns` VALUES (8000,'2020-07-03 00:00:00','2020-07-03 00:00:00',1,1),(10000,'2019-04-05 00:00:00','2019-04-05 00:00:00',1,2),(500,'2019-12-10 00:00:00','2019-12-10 00:00:00',1,3),(2135,'2020-12-17 00:00:00','2020-12-17 00:00:00',1,4),(500,'2019-02-09 00:00:00','2019-02-09 00:00:00',1,5),(5040,'2020-12-11 00:00:00','2020-12-11 00:00:00',1,6),(4000,'2020-05-07 00:00:00','2020-05-07 00:00:00',1,7),(8000,'2020-07-29 00:00:00','2020-07-29 00:00:00',1,8);
/*!40000 ALTER TABLE `returns` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `state`
--

DROP TABLE IF EXISTS `state`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `state` (
  `idstate` int NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idstate`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `state`
--

LOCK TABLES `state` WRITE;
/*!40000 ALTER TABLE `state` DISABLE KEYS */;
INSERT INTO `state` VALUES (1,'Active'),(2,'Inactive');
/*!40000 ALTER TABLE `state` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `timeslot`
--

DROP TABLE IF EXISTS `timeslot`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `timeslot` (
  `idtimeSlot` int NOT NULL AUTO_INCREMENT,
  `from` datetime NOT NULL,
  `to` datetime NOT NULL,
  PRIMARY KEY (`idtimeSlot`),
  UNIQUE KEY `from_to_uniq` (`from`,`to`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `timeslot`
--

LOCK TABLES `timeslot` WRITE;
/*!40000 ALTER TABLE `timeslot` DISABLE KEYS */;
INSERT INTO `timeslot` VALUES (1,'2020-12-04 09:00:00','2021-12-04 10:00:00'),(2,'2020-12-04 10:00:00','2021-12-04 11:00:00'),(3,'2020-12-04 11:00:00','2021-12-04 12:00:00'),(4,'2020-12-04 12:00:00','2021-12-04 13:00:00'),(5,'2020-12-04 13:00:00','2021-12-04 14:00:00'),(6,'2020-12-04 14:00:00','2021-12-04 15:00:00'),(7,'2020-12-05 09:00:00','2021-12-05 10:00:00'),(8,'2020-12-05 10:00:00','2021-12-05 11:00:00'),(9,'2020-12-05 11:00:00','2021-12-05 12:00:00'),(10,'2020-12-05 12:00:00','2021-12-05 13:00:00'),(11,'2020-12-05 13:00:00','2021-12-05 14:00:00'),(12,'2020-12-05 14:00:00','2021-12-05 15:00:00');
/*!40000 ALTER TABLE `timeslot` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-12-12  8:58:04
